import * as firebase from "firebase";
import "firebase/firestore";

//This normally would be somewhere safe :)
var firebaseConfig = {
  apiKey: "AIzaSyAoyZR_maINRpdIHJcUXfKPiCAG3vTG7P4",
  authDomain: "vincit-demo.firebaseapp.com",
  databaseURL: "https://vincit-demo.firebaseio.com",
  projectId: "vincit-demo",
  storageBucket: "vincit-demo.appspot.com",
  messagingSenderId: "921294674982",
  appId: "1:921294674982:web:c20a7ceb30bfabb4"
};

firebase.initializeApp(firebaseConfig);

export default (!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase);
