/*
Created by: Daniel Davidson
This script is meant to populate a firestore database with 
articles and videos scraped from https://www.vincit.com 
*/

const request = require("request");
const cheerio = require("cheerio");
const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccount.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

async function getBlogData() {
  try {
    request("https://www.vincit.com/blog", async (error, response, html) => {
      if (!error && response.statusCode == 200) {
        let blogs = [];
        const $ = cheerio.load(html);
        let promise = new Promise(resolve => {
          //Loop though and get all articles
          $("a.blog-card").each(async (i, el) => {
            const blogURL = $(el).attr("href");
            const creatorCard = $(el).find("div.article-card__author");
            const authorDetails = creatorCard.find("div.article-card__author-details");
            const authorName = authorDetails
              .find("div.article-card__name")
              .text()
              .trim();
            const category = $(el)
              .find("div.blog-card__category")
              .text()
              .trim();
            const title = $(el)
              .find("h2.article-card__title")
              .text()
              .trim();
            const subTitle = $(el)
              .find("div.article-card__snippet")
              .text()
              .trim();
            const timeStamp = authorDetails
              .find("div.article-card__date")
              .text()
              .trim();

            let newBlog = {
              category: category,
              title: title,
              subTitle: subTitle,
              creatorName: authorName,
              blogURL: blogURL,
              timeStamp: timeStamp
            };

            blogs.push(newBlog);
          });
          //Loop through and get all Blurbs
          $("div.blog-card").each(async (i, el) => {
            let blogURL = $(el)
              .find("a")
              .attr("href");

            const category = $(el)
              .find("div.blog-card__category")
              .text()
              .trim();
            const title = $(el)
              .find("p")
              .text()
              .trim();
            const timeStamp = $(el)
              .find("div.blurb-card__date")
              .text()
              .trim();
            if (!blogURL) {
              blogURL = "https://www.vincit.com/blog";
            }
            let newBlog = {
              category: category,
              title: title,
              subTitle: "",
              creatorName: "Vincit",
              blogURL: blogURL,
              timeStamp: timeStamp
            };

            blogs.push(newBlog);
          });
          resolve(blogs);
        });
        //Upload to firebase
        let result = await promise;
        uploadToFirebase(result);
      }
    });
  } catch (error) {
    console.log("Error: ", error);
  }
}
function uploadToFirebase(blogs) {
  var blogRef = admin.firestore().collection("Blogs");

  blogs.forEach(blog => {
    blogRef.add(blog).then(ref => {
      console.log("Uploaded document: ", ref.id);
    });
  });
}
(async () => {
  await getBlogData();
})();
