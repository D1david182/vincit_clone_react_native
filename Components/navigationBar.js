/*
  Created by: Daniel Davidson
  This class creates a custom navigation bar with logo and title. 
*/

import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";

let navBar = () => {
  return (
    <View style={styles.navBar}>
      <Text style={styles.titleLabel}>Vincit</Text>
      <Image style={styles.topLogo} source={require("../assets/logo.png")} />
    </View>
  );
};

let styles = StyleSheet.create({
  navBar: {
    flexDirection: "row",
    backgroundColor: "#EE442B",
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOpacity: 0.1,
    shadowOffset: { width: 0, height: 2 }
  },
  titleLabel: {
    color: "white",
    fontSize: 28,
    fontWeight: "700"
  },
  topLogo: {
    justifyContent: "flex-end",
    height: 45,
    width: 45,
    resizeMode: "cover"
  }
});
export default navBar;
