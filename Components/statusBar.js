/*
  Created by: Daniel Davidson
  This class creates a small bar on the top of iOS devices only. 
*/

import React from "react";
import { View, StyleSheet } from "react-native";

let statusBar = () => {
  return <View style={styles.statusBar} />;
};

let styles = StyleSheet.create({
  statusBar: {
    height: 50,
    backgroundColor: "#EE442B"
  }
});

export default statusBar;
