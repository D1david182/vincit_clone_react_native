/*
  Created by: Daniel Davidson
  This class contains the main logic of the app. It consists of the top flatlist of categories 
  and the bottom list of blogs. 
*/

import { View, StyleSheet, FlatList } from "react-native";
import React, { Component } from "react";
import CategoryCell from "./categoryCell";
import BlogCell from "./blogCell";
import BlogDataSource from "../DataSources/BlogsDataSource";

export default class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      categories: [
        { title: "Design", selected: true },
        { title: "Culture", selected: false },
        { title: "Technology", selected: false },
        { title: "Blurbs", selected: false }
      ],
      blogs: [],
      selectedCategory: "Design"
    };
  }
  componentDidMount() {
    this.retrieveData();
  }

  /* 
  Used to produce single select functionality in the categories flatlist
  Loop through and deselect all, then select only the passed in id
  Currently not refreshing flatlist for reasons unknown :(
  */
  handleSelectCategory = id => {
    const { categories } = this.state;
    let copy = categories;
    copy.forEach(elem => {
      elem.selected = false;
      if (elem.title === id) {
        elem.selected = true;
      }
    });
    //This is kinda a hack but havent figured out to refresh all category cells without removing them all first
    this.setState({ categories: [] }, () => {
      this.setState({ categories: copy, selectedCategory: id, loading: true }, () => {
        this.retrieveData();
      });
    });
  };

  //Get all blogs from firebase with given selected category
  async retrieveData() {
    let dataSource = new BlogDataSource();
    blogs = await dataSource.retrieveBlogsByCategory(this.state.selectedCategory);
    this.setState({
      blogs: blogs,
      loading: false
    });
  }

  renderItem = ({ item }) => <CategoryCell id={item.title} handleSelect={this.handleSelectCategory} category={item} />;

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.categoryContainer}>
          <FlatList
            contentContainerStyle={styles.categorylist}
            extraData={this.state}
            data={this.state.categories}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => item.title.toString()}
            horizontal={true}
          />
        </View>
        <View style={styles.container}>
          <FlatList
            refreshing={this.state.loading}
            contentContainerStyle={styles.blogList}
            extraData={this.state}
            data={this.state.blogs}
            renderItem={({ item }) => <BlogCell blog={item} />}
            keyExtractor={(item, index) => item.title.toString()}
            onRefresh={() => this.retrieveData()}
          />
        </View>
      </View>
    );
  }
}
//Styles for all components
let styles = StyleSheet.create({
  categoryContainer: {
    marginTop: 0,
    flexDirection: "row",
    justifyContent: "center",
    height: 80,
    backgroundColor: "white"
  },
  container: {
    flexDirection: "column",
    flex: 1,
    backgroundColor: "white",
    marginBottom: 20
  },
  categorylist: {
    marginTop: 20,
    flex: 1,
    justifyContent: "space-evenly"
  },
  blogList: {
    marginTop: 20,
    marginBottom: 20
  }
});
