 ## Hello!
 ### Thanks for trying out the project.
 
 This project is a clone of the website https://www.vincit.com with React Native.
 The blog content is webscraped with a node.js script found in the root directory of this project and uploaded to a Firestore database.
 
 If you wish to run the script and update the database you will need to setup your own firestore database and supply the correct serviceAccount.json file which
 is retrieved from your firebase console.
 
 To simply run the app, clone this repo, CD into the project directory, run **npm install** and then **npm start**. You will need xcode installed to run it on a simulator. 
 
 #### If any errors occur you may need to:
 1: Make sure you installed all dependencies.  
 2. Email daniel.davidson.dev@gmail.com becuase he probably messed something up.

 You should be able to see an app consiting of various blogs as shown below. 
 
![alt text](https://danielsrandom.s3.amazonaws.com/smartmockups_jym7bxci.png)

