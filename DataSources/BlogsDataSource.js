/*
  Created by: Daniel Davidson
  This class acts as a datasource and connection to a firebase firestore database.
*/

import firebase from "../firebase";

export default class BlogsDataSource {
  async retrieveBlogsByCategory(category) {
    try {
      let blogs = [];
      let blogsRef = firebase.firestore().collection("Blogs");
      if (category == "Blurbs") {
        category = "Blurb";
      }
      const snapshot = await blogsRef.where("category", "==", category).get();
      await snapshot.forEach(doc => {
        let newBlog = {
          title: doc.data().title,
          subTitle: doc.data().subTitle,
          category: doc.data().category,
          creatorName: doc.data().creatorName,
          timeStamp: doc.data().timeStamp,
          blogURL: doc.data().blogURL
        };
        blogs.push(newBlog);
      });

      return blogs;
    } catch (error) {
      console.log("Error downloading blogs", error);
      return [];
    }
  }
}
