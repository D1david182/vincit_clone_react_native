/*
  Created by: Daniel Davidson
  This class compiles all main components of the app into one. 
*/

import React, { Component } from "react";
import { StyleSheet, View, Platform } from "react-native";
import StatusBar from "../Components/statusBar";
import NavigationBar from "../Components/navigationBar";
import HomeFeed from "../Components/homeFeed";

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        {Platform.OS === "ios" && <StatusBar />}
        <NavigationBar />
        <HomeFeed style={styles.categoryList} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  categoryList: {
    backgroundColor: "yellow",
    flexDirection: "row",
    width: "100%",
    height: 100,
    flex: 1
  }
});

export default HomeScreen;
