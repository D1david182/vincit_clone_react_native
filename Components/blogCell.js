/*
  Created by: Daniel Davidson
  This class is the main cell for the blog and changes color based on if the 
  category is a blurb or regular article. 
*/

import { View, Text, StyleSheet, TouchableOpacity, Image, Linking } from "react-native";
import React, { Component } from "react";

class BlogCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      subTitle: "",
      category: "",
      creatorName: "",
      timeStamp: "",
      blogURL: ""
    };
  }
  componentDidMount() {
    this.setState({
      title: this.props.blog.title,
      subTitle: this.props.blog.subTitle,
      category: this.props.blog.category,
      creatorName: this.props.blog.creatorName,
      timeStamp: this.props.blog.timeStamp,
      blogURL: this.props.blog.blogURL
    });
  }
  render() {
    return (
      <TouchableOpacity
        style={this.state.category !== "Blurb" ? styles.standardCell : styles.blurbCell}
        onPress={() => {
          //Open website to given vincit blog link
          Linking.openURL(this.state.blogURL).catch(err => console.error("Error occurred opening website", err));
        }}
      >
        <Text style={this.state.category !== "Blurb" ? styles.categoryLabelStandard : styles.categoryLabelBlurb}>#{this.state.category}</Text>
        <Text style={this.state.category !== "Blurb" ? styles.titleLabelStandard : styles.titleLabelBlurb}>{this.state.title}</Text>
        <Text style={this.state.category !== "Blurb" ? styles.subTitleLabelStandard : styles.subTitleLabelBlurb}>{this.state.subTitle}</Text>
        <View style={styles.creatorAttributesContainer}>
          <Image style={styles.personImage} source={require("../assets/logo.png")} />
          <Text style={this.state.category !== "Blurb" ? styles.creatorNameLabelStandard : styles.creatorNameLabelBlurb}>{this.state.creatorName}</Text>
          <Text style={this.state.category !== "Blurb" ? styles.timeStampLabelStandard : styles.timeStampLabelBlurb}>{this.state.timeStamp}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

let styles = StyleSheet.create({
  blurbCell: {
    margin: 10,
    backgroundColor: "#EE442B",
    alignItems: "flex-start",
    justifyContent: "center",
    borderRadius: 9,
    paddingLeft: 12,
    paddingRight: 12,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 1 }
  },
  standardCell: {
    margin: 10,
    backgroundColor: "white",
    alignItems: "flex-start",
    justifyContent: "center",
    borderRadius: 9,
    paddingLeft: 12,
    paddingRight: 12,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 1 }
  },
  titleLabelStandard: {
    marginTop: 20,
    color: "black",
    fontSize: 28,
    fontWeight: "600"
  },
  subTitleLabelStandard: {
    marginTop: 12,
    color: "black",
    fontSize: 24,
    fontWeight: "300"
  },
  creatorNameLabelStandard: {
    marginLeft: 8,
    color: "black",
    fontSize: 20,
    fontWeight: "500"
  },
  categoryLabelStandard: {
    marginTop: 8,
    color: "#EE442B",
    fontSize: 22,
    fontWeight: "600"
  },
  timeStampLabelStandard: {
    marginTop: 3,
    marginLeft: 10,
    color: "gray",
    fontSize: 16,
    fontWeight: "300"
  },
  titleLabelBlurb: {
    marginTop: 20,
    color: "white",
    fontSize: 28,
    fontWeight: "600"
  },
  subTitleLabelBlurb: {
    marginTop: 12,
    color: "white",
    fontSize: 24,
    fontWeight: "300"
  },
  creatorNameLabelBlurb: {
    marginLeft: 8,
    color: "white",
    fontSize: 20,
    fontWeight: "500"
  },
  categoryLabelBlurb: {
    marginTop: 8,
    color: "black",
    fontSize: 22,
    fontWeight: "600"
  },
  timeStampLabelBlurb: {
    marginTop: 3,
    marginLeft: 10,
    color: "black",
    fontSize: 16,
    fontWeight: "300"
  },
  creatorAttributesContainer: {
    marginTop: 15,
    marginBottom: 12,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  personImage: {
    height: 30,
    width: 30,
    resizeMode: "cover",
    borderRadius: 15
  }
});

export default BlogCell;
