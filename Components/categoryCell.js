/*
  Created by: Daniel Davidson
  This class is used for the category filters. 
*/

import { Text, StyleSheet, TouchableOpacity } from "react-native";
import React, { Component } from "react";

class CategoryCell extends Component {
  constructor(props) {
    super(props);
    this.state = { isSelected: false, title: "" };
  }
  componentDidMount() {
    let { category } = this.props;
    this.setState({
      title: category.title,
      isSelected: category.selected
    });
  }
  render() {
    const { handleSelect } = this.props;
    const category = this.props.category.title;
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ isSelected: !this.state.isSelected });
          handleSelect(this.state.title);
        }}
        style={this.state.isSelected ? styles.backgroundSelected : styles.backgroundNotSelected}
      >
        <Text style={this.state.isSelected ? styles.textSelected : styles.textNotSelected}>{category}</Text>
      </TouchableOpacity>
    );
  }
}

let styles = StyleSheet.create({
  backgroundSelected: {
    backgroundColor: "#EE442B",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 15,
    height: 35,
    paddingLeft: 8,
    paddingRight: 8,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 1 }
  },
  backgroundNotSelected: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 15,
    height: 35,
    paddingLeft: 8,
    paddingRight: 8,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 1 }
  },
  textSelected: {
    color: "white",
    fontSize: 18,
    fontWeight: "700"
  },
  textNotSelected: {
    color: "black",
    fontSize: 18,
    fontWeight: "700"
  }
});

export default CategoryCell;
